import axios from 'axios';

let $axios = axios.create({
	baseURL: 'http://localhost:9090/',
});

/**
 * 업로드하려는 파일이 등록가능한 파일인지,
 * 불가능하다면 어떤 상태인지
 * 확인한다.
 */
function uploadFileCheck(hashValue) {
	return $axios.get(`/api/upload/check/${hashValue}`);
}

/**
 * 파일이 정상적으로 업로드 된 뒤에
 * API서버에게 대상파일의 DB작업을 시작시킨다.
 */
function putBatchData(hashValue) {
	return $axios.put(`/api/upload/data/${hashValue}`);
}
export { uploadFileCheck, putBatchData };
