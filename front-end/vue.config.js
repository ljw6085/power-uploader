module.exports = {
	devServer: {
		overlay: false,
		proxy: {
			'^/api': {
				target: 'http://localhost:9090', // 개발서버
			},
		},
	},
};
