package com.uploader.app.data.repository;

import com.uploader.app.common.enums.SaveStatus;
import com.uploader.app.data.entity.UploadedInfo;
import com.uploader.app.data.entity.User;
import lombok.SneakyThrows;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.junit4.SpringRunner;

import javax.sql.DataSource;

import java.sql.Connection;
import java.sql.SQLException;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;

@RunWith(SpringRunner.class)
//@DataJpaTest
@SpringBootTest
class UserRepositoryTest {
    @Autowired
    DataSource dataSource;

    @Autowired
    UserRepository userRepository;

    @Autowired
    UploadedInfoRepository uploadedInfoRepository;

    @Test
    public void userInsert(){
//
        Long testId = 1L;
        String testFirstName = "Jongwook";
        String testlastName = "Lee";
        String testEmail = "ljw6085@gmail.com";
        User user = User.builder()
                        .id(testId)
                        .firstName(testFirstName)
                        .lastName(testlastName)
                        .email(testEmail)
                        .build();
        User newUser = userRepository.save(user);
        assertThat(newUser).isNotNull();

        User existingUser = userRepository.findById(testId).get();
        assertThat(existingUser).isNotNull();
    }
    @Test
    public void uploadedInfoTest(){
        String hashValue = "fileHashValue";
       UploadedInfo uploadedInfo = UploadedInfo.builder()
               .filePath("filePath")
               .fileHashValue(hashValue)
               .failCount(0)
               .failRowNumbers("[1,2]")
               .lastedInsertRawData("1,jongwlee,jongwlee@")
               .lastedStatusMessage("")
               .status(SaveStatus.SUCCESS)
               .build();
        UploadedInfo newUploadedInfo = uploadedInfoRepository.save(uploadedInfo);

        assertThat(newUploadedInfo).isNotNull();

        UploadedInfo getUploadedInfo = uploadedInfoRepository.findByFileHashValue(hashValue).orElseGet(UploadedInfo::new);

        assertThat(getUploadedInfo.getSeq()).isNotNull();


    }
}