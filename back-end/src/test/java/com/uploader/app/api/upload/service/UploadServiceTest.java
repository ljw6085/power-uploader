package com.uploader.app.api.upload.service;

import com.uploader.app.common.enums.SaveStatus;
import com.uploader.app.common.exception.ValueReturnException;
import com.uploader.app.data.dto.UserDTO;
import com.uploader.app.data.dto.BatchResultDTO;
import com.uploader.app.data.entity.User;
import com.uploader.app.data.repository.UploadedInfoRepository;
import com.uploader.app.data.repository.UserRepository;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;

@RunWith(SpringRunner.class)
//슬라이싱테스트
//@DataJpaTest
@SpringBootTest
class UploadServiceTest {

    @Autowired UploadService uploadService;

    @Autowired UserRepository userRepository;

    @Autowired
    UploadedInfoRepository uploadedInfoRepository;




    String hashValue = "abcd1234!";

    @Test
    public void firstFileUpload(){
        //맨 처음 파일 업로드할때 데이터 확인
        try {
            BatchResultDTO batchResultDTO =uploadService.checkUploadedFile(hashValue);
            assertThat(batchResultDTO.getStatus()).isEqualTo(SaveStatus.SUCCESS);

        } catch (Exception e) {
            e.printStackTrace();
        }

    }
    @Test
    public void fileUploadHistorySave(){
        // 파일 업로드내역 저장 및 서버에 파일 저장
        MockMultipartFile file = new MockMultipartFile("file","test.txt" , "text/plain" , "hello file".getBytes());
        try {
            uploadService.transferFile(file, hashValue);
            assertThat(uploadedInfoRepository.findByFileHashValue(hashValue)).isNotNull();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public List<UserDTO> FileReadTest(){
        // DB Insert 요청시,
        // 파일을 읽은 후 DB Insert 준비를 하는 과정


        String testFilePath = "/Users/leejw/Downloads/카카오페이 웹(풀스택) 개발 과제/카카오페이 웹(풀스택) 개발 과제 - 데이터셋.csv";
//        String testFilePath = "/Users/leejw/Downloads/카카오페이 웹(풀스택) 개발 과제/카카오페이 웹(풀스택) 개발 과제 - 데이터셋_ERROR_VER.csv";
        try(FileInputStream fis = new FileInputStream(testFilePath);){

            List<UserDTO> result = uploadService.readUserListFromFile(fis);

            assertThat(result.size()).isEqualTo(100000);

//            UserDTO targetErrorData = result.get(2);
//            assertThat(targetErrorData.getId()).isNull();
//            assertThat(targetErrorData.getRawData()).isNotNull();
//            assertThat(targetErrorData.getRawData()).isEqualTo(",Deloria,Daegal,Deloria.Daegal@yopmail.com");

            // 정상적인 데이터가 아닌경우 rawData에 데이터가 들어가 있을것.
//            assertThat(result.get(1).getRawData()).isNotNull();
            assertThat(result.get(1).getRawData()).isNull();

            return result;
        } catch (FileNotFoundException fileNotFoundException) {
            fileNotFoundException.printStackTrace();
        } catch (IOException ioException) {
            ioException.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }



    @Test
    public void batchInsertTest(){
        //파일을 읽어서 JVM 메모리에 올려놓은 데이터들을 DB에 Insert 한다

        List<UserDTO> result = this.FileReadTest();

        assertThat(result).isNotNull();

        try {
            //테스트를위해 전체삭제 후 진행
            userRepository.deleteAll();

            // 배치insert
            BatchResultDTO batchResultDTO = userRepository.batchInsert(hashValue, result, 1000);

            // 에러건수가 있는지 확인
            assertThat(batchResultDTO.hasError()).isFalse();

            //전체조회
            List<User> insertList = userRepository.findAll();
            //카운트확인
            assertThat(insertList.size()).isEqualTo(result.size());

            assertThat(true).isTrue();
        }catch (ValueReturnException ve ){
            // 배치업데이트시 오류가 난 경우( 중복 키, DB관련 오류 )
            // batchSize만큼 commit이 안되기 때문에,
            // 마지막 insert된 시점 이후로는 작업을 중단하고
            // 마지막 insert된 데이터를 반환한다.
            BatchResultDTO erroDTO = (BatchResultDTO) ve.getValue();
            assertFalse(true);

        } catch (Exception e) {
            e.printStackTrace();
            assertFalse(true);
        }

    }
}