package com.uploader.app.data.repository;

import com.uploader.app.data.entity.UploadedInfo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UploadedInfoRepository extends JpaRepository<UploadedInfo,Long> {
    Optional<UploadedInfo> findByFileHashValue(String fileHashValue);
}
