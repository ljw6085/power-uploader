package com.uploader.app.data.dto;

import lombok.*;

import javax.persistence.Id;


@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class UserDTO {
    private Long id;
    private String firstName;
    private String lastName;
    private String email;

    private String rawData;
    private Integer rowNumber;

    public String createRawData(){
        return id+","+firstName+","+lastName+","+email;
    }
}
