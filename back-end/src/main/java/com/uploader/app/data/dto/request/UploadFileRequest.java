package com.uploader.app.data.dto.request;


import lombok.*;
import org.springframework.web.multipart.MultipartFile;

@Builder
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class UploadFileRequest {

    private MultipartFile file;
    private String hashValue;
}
