package com.uploader.app.data.dto;

import com.uploader.app.common.enums.SaveStatus;
import lombok.*;

import javax.persistence.GeneratedValue;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class BatchResultDTO {
    String hashValue;

    List<String> failRowNumbers;

    String lastedInsertRawData;
    String errorMessage;

    Integer totalCount;
    Integer failCount;
    Integer successCount;

    SaveStatus status;

    boolean isEnd;

    public List<String> getFailRowNumbers(){
        return this.failRowNumbers == null ? new ArrayList<String>() : this.failRowNumbers;
    }
    public boolean hasError(){
        return (this.failRowNumbers != null && this.failRowNumbers.size() > 0)
                || (this.errorMessage != null && !this.errorMessage.isEmpty());
    }

    public SaveStatus getStatus(){
        return hasError() ? SaveStatus.FAIL : SaveStatus.SUCCESS;
    }

}
