package com.uploader.app.data.repository;

import com.uploader.app.data.dto.UserDTO;
import com.uploader.app.data.dto.BatchResultDTO;

import java.util.List;

public interface UserRepositoryCustom {
    public BatchResultDTO batchInsert(String hashValue, List<UserDTO> dataList, int batchSize) throws Exception;
}
