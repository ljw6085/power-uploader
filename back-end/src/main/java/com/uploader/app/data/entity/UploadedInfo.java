package com.uploader.app.data.entity;

import com.uploader.app.common.enums.SaveStatus;
import lombok.*;
import org.hibernate.annotations.CreationTimestamp;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
public class UploadedInfo {

    @Id
    @GeneratedValue
    Long seq;
    String fileHashValue;
    String filePath;
    @Enumerated(EnumType.STRING)
    SaveStatus status;
    String lastedInsertRawData;
    String lastedStatusMessage;

    String failRowNumbers;

    Integer successCount;
    Integer failCount;
    Integer totalCount;

    @CreationTimestamp
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    LocalDateTime lastedUpdateDate;
}
