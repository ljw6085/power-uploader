package com.uploader.app.data.repository;

import com.uploader.app.common.CacheMap;
import com.uploader.app.common.enums.ResultCodes;
import com.uploader.app.common.exception.BusinessException;
import com.uploader.app.common.exception.ValueReturnException;
import com.uploader.app.data.dto.UserDTO;
import com.uploader.app.data.dto.BatchResultDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.ParameterizedPreparedStatementSetter;

import javax.sql.DataSource;
import java.sql.BatchUpdateException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class UserRepositoryImpl implements UserRepositoryCustom {

    @Autowired DataSource dataSource;
    @Autowired CacheMap cache;

    @Override
    public BatchResultDTO batchInsert(String hashValue, List<UserDTO> dataList, int batchSize) throws Exception {
        ArrayList<String>  error = new ArrayList<>();
        String sql = "insert user (id, first_name, last_name, email) values(?,?,?,?)";

        String lastCommitRawData = "";

        Connection con = dataSource.getConnection();
        PreparedStatement ps = con.prepareStatement(sql);
        con.setAutoCommit(false);
        Integer cursor  = 0;
        Integer totalCount = dataList.size();
        Integer failCount = 0;
        Integer successCount = 0;
        BatchResultDTO batchResultDTO = BatchResultDTO.builder()
                .isEnd(false)
                .totalCount(totalCount)
                .build();

        // 배치 진행결과를 사용자에게 확인시키기 위해 캐시맵에 담는다.
        cache.put(hashValue, batchResultDTO );
        try {
            for (UserDTO userDTO : dataList) {
                cursor++;
                if (userDTO.getId() == null) {
                    // id가 null이라는 것은 업로드한 파일을 읽었을 때, 이미 유효하지 않은 데이터이다.
                    // 이 경우 데이터 정합성의 문제이므로 어떤 형태로 들어왔는지 확인 후
                    // Insert 는 skip하고, 마지막에 사용자에게 다시 전달해준다.
                    // 일단 최대 5개만 보여준다.
                    if( error.size() < 5 ) error.add(userDTO.getRowNumber()+"");
                    failCount++;

                    // 실시간으로 정보를 update
                    batchResultDTO.setFailCount(failCount);
                } else {
                    try{
                        ps.setLong(1, userDTO.getId());
                        ps.setString(2, userDTO.getFirstName());
                        ps.setString(3, userDTO.getLastName());
                        ps.setString(4, userDTO.getEmail());

                        // addBatch에 담기
                        ps.addBatch();
                        // 파라미터 Clear
                        ps.clearParameters();

                        if (cursor % batchSize == 0 || cursor == dataList.size()) {
                            ps.executeBatch();
                            ps.clearBatch();
                            con.commit();

                            // 커밋시 마지막 데이터 저장
                            lastCommitRawData = userDTO.createRawData();

                            // 실시간으로 정보를 update
                            successCount = cursor - failCount;
                            batchResultDTO.setSuccessCount(successCount);

                        }
                    }catch (BatchUpdateException batchUpdateException ){
                        // 배치 업데이트중 예외발생시
                        // 익셉션을 발생시켜 더 이상 작업을 중단시킨다.
                        throw batchUpdateException;
                    }

                }
            }
        }catch (Exception e ){
            // 배치 업데이트 중 발생한 에외에서는 작업을 중단 한다.
            // 특정 데이터때문에 batchSize만큼 정상적인데이터이어도 커밋이 안됨.
            // 마지막 commit시점 이후로 즉시 중단 후 관련 데이터를 전달한다.
            // (=>마지막 저장된 데이터와, 정합성검사에서 걸린 데이터들의 목록을 전달)
            batchResultDTO.setFailRowNumbers(error);
            batchResultDTO.setTotalCount(totalCount);
            batchResultDTO.setSuccessCount(successCount);// cursor는 실패든 성공이든 무조건 체크된다. 그래서 커밋당시의 성공카운트를 넣어야한다.
            batchResultDTO.setFailCount(failCount);
            batchResultDTO.setLastedInsertRawData(lastCommitRawData);
            batchResultDTO.setErrorMessage(e.getMessage());
            throw new ValueReturnException(batchResultDTO, e.getMessage());
        }finally {
            if( ps!= null){
                ps.close();
            }
            if(con!=null){
                con.close();
            }

            // 작업이끝나면 캐싱된 리소스를 반환-끝남을 알림
            batchResultDTO.setEnd(true);
        }

        batchResultDTO.setFailRowNumbers(error);
        batchResultDTO.setTotalCount(totalCount);
        batchResultDTO.setSuccessCount(successCount);// cursor는 실패든 성공이든 무조건 체크된다. 그래서 커밋당시의 성공카운트를 넣어야한다,
        batchResultDTO.setFailCount(failCount);
        batchResultDTO.setLastedInsertRawData(lastCommitRawData);

        return batchResultDTO;
    }

}
