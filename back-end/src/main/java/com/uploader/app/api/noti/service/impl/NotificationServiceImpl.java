package com.uploader.app.api.noti.service.impl;

import com.uploader.app.api.noti.service.NotificationService;
import com.uploader.app.common.CacheMap;
import com.uploader.app.data.dto.BatchResultDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;


@Service
public class NotificationServiceImpl implements NotificationService {

    @Autowired private CacheMap cacheMap;

    @Override
    public BatchResultDTO currentBatchStatus(String hashValue) throws Exception {
       BatchResultDTO batchResultDTO  = cacheMap.get(hashValue);
       //세션이 끊겼을때 remove 해야한다.
//       if( batchResultDTO.getHashValue() != null && batchResultDTO.isEnd() )
//           cacheMap.remove(hashValue);
        return batchResultDTO;
    }
}
