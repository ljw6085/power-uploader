package com.uploader.app.api.upload.service.impl;

import com.uploader.app.api.upload.service.UploadService;
import com.uploader.app.common.CacheMap;
import com.uploader.app.common.enums.ResultCodes;
import com.uploader.app.common.enums.SaveStatus;
import com.uploader.app.common.exception.BusinessException;
import com.uploader.app.common.exception.ValueReturnException;
import com.uploader.app.data.dto.UserDTO;
import com.uploader.app.data.dto.BatchResultDTO;
import com.uploader.app.data.entity.UploadedInfo;
import com.uploader.app.data.repository.UploadedInfoRepository;
import com.uploader.app.data.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.*;
import java.time.LocalDateTime;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

@Service
public class UploadServiceImpl implements UploadService {

    @Autowired
    private UploadedInfoRepository uploadedInfoRepository;

    @Autowired
    private UserRepository userRepository;

    private final int DELIM_TOKENIZER = 1;
    private final int DELIM_SPLIT     = 2;

    @Value("${file.upload.temp.path}")
    private String ROOT_PATH;

    @Override
    public BatchResultDTO checkUploadedFile(String hashValue) throws Exception {
        UploadedInfo uploadedInfo = uploadedInfoRepository.findByFileHashValue(hashValue).orElseGet(UploadedInfo::new);

        BatchResultDTO batchResultDTO = null;
        if( uploadedInfo.getSeq() == null){
            batchResultDTO = BatchResultDTO.builder().status(SaveStatus.NULL).build();
        }else{
            batchResultDTO = BatchResultDTO.builder()
                    .failRowNumbers(
                            Arrays.stream( uploadedInfo.getFailRowNumbers().split(","))
                                    .map(v->v.replaceAll("[^0-9]",""))
                                    .collect(Collectors.toList())  )
                    .lastedInsertRawData(uploadedInfo.getLastedInsertRawData())
                    .status(uploadedInfo.getStatus())
                    .totalCount(uploadedInfo.getTotalCount())
                    .failCount(uploadedInfo.getFailCount())
                    .successCount(uploadedInfo.getSuccessCount())
                    .isEnd(
                            (uploadedInfo.getStatus().equals(SaveStatus.SUCCESS) ||
                                    uploadedInfo.getStatus().equals(SaveStatus.FAIL))
                    )
                    .errorMessage(
                            uploadedInfo.getStatus().equals(SaveStatus.SUCCESS) ? "이미 업로드가 완료된 파일입니다.":
                                    uploadedInfo.getStatus().equals(SaveStatus.FAIL) ? "실패이력이 있는 파일입니다. 파일 확인 후 다시시도 해주세요."
                                            :""
                    ).build();
        }
        return batchResultDTO;
    }

    @Override
    public void transferFile(MultipartFile file, String hashValue) throws Exception {
        String filePath = ROOT_PATH + hashValue;

        UploadedInfo uploadedInfo = uploadedInfoRepository.findByFileHashValue(hashValue).orElseGet(UploadedInfo::new);

        if( uploadedInfo.getSeq() == null){
            File tmpFile = new File( filePath);
            file.transferTo(tmpFile);

            // 데이터 저장할 준비 - 업로드 목록 DB insert
            uploadedInfoRepository.save(
                    UploadedInfo.builder()
                            .status(SaveStatus.NULL)
                            .fileHashValue(hashValue)
                            .filePath(filePath)
                        .build()
            );

        }else{
            if( uploadedInfo.getStatus().equals(SaveStatus.SUCCESS) ){
                // 이미 완료된 파일
                throw new BusinessException(ResultCodes.ALREADY_INSERT_FILE_ERROR);
            }else if (uploadedInfo.getStatus().equals(SaveStatus.FAIL)){
                String msg = "실패된 이력이 있습니다. 아래정보 확인 후 재시도 해주세요.";
                msg +="\n - 오류 데이터 Line 번호 : %s ";
                msg +="\n - 오류 데이터 건수 : %d";
                msg +="\n - 정상적으로 입력된 마지막 데이터 : %s ";
                msg +="\n - 실패 메시지 : [%s]";
                throw new BusinessException(
                        String.format(msg
                                , uploadedInfo.getFailRowNumbers()
                                , uploadedInfo.getFailCount()
                                , ( uploadedInfo.getLastedInsertRawData() == null || uploadedInfo.getLastedInsertRawData().isEmpty()
                                        ? "없음" : uploadedInfo.getLastedInsertRawData() )
                                , ( uploadedInfo.getLastedStatusMessage() == null
                                        ? "" : uploadedInfo.getLastedStatusMessage())
                ));

            }
        }
    }

    @Autowired CacheMap cache;
    @Override
    public BatchResultDTO insertDataFromFile(String hashValue) throws Exception {

        UploadedInfo uploadedInfo = uploadedInfoRepository.findByFileHashValue(hashValue).orElseGet(UploadedInfo::new);
        if( uploadedInfo.getSeq() == null ){
            throw new BusinessException(ResultCodes.DATAFILE_NOT_FOUND);
        }

        File file = new File ( uploadedInfo.getFilePath() );
        BatchResultDTO batchResultDTO = BatchResultDTO.builder().status(SaveStatus.NULL).build();
        try(FileInputStream fis = new FileInputStream(file);){

            List<UserDTO> dataListFromFile = this.readUserListFromFile(fis);
            batchResultDTO = userRepository.batchInsert(hashValue, dataListFromFile, 1000);

        }catch ( ValueReturnException ve ){
            // 배치업데이트시 오류가 난 경우( 중복 키, DB관련 오류 )
            // batchSize만큼 commit이 안되기 때문에,
            // 마지막 insert된 시점 이후로는 작업을 중단하고
            // 마지막 insert된 데이터를 반환한다.
            batchResultDTO = (BatchResultDTO) ve.getValue();
        }catch (Exception e){
            // 이 서비스에 들어왔다는것은, 사용자는 현재 stomp를 이용하여 상태값을 캐치해가고있다.
            // 따라서 오류가났을때에도 같은 상태값을 가져가게 하기 위해 캐시맵에 오류 상태값을 세팅해주어야 한다.
            if( cache.get(hashValue) == null ){
                batchResultDTO.setStatus(SaveStatus.FAIL);
                batchResultDTO.setErrorMessage(e.getMessage());
                cache.put(hashValue , batchResultDTO);
            }
            else{
                cache.get(hashValue).setErrorMessage(e.getMessage());
            }
        }

        // DB Batch Insert 파일별 상태 결과 저장
        if( batchResultDTO.hasError() ){
            uploadedInfo.setStatus(SaveStatus.FAIL);
            uploadedInfo.setLastedStatusMessage(batchResultDTO.getErrorMessage());
        }else{
            // 오류데이터가 없거나, 오류메시지가 없는경우에만 성공처리
            uploadedInfo.setStatus(SaveStatus.SUCCESS);
            uploadedInfo.setLastedStatusMessage(SaveStatus.SUCCESS.getStatus());
        }

        uploadedInfo.setLastedUpdateDate(LocalDateTime.now());
        uploadedInfo.setLastedInsertRawData(batchResultDTO.getLastedInsertRawData());
        uploadedInfo.setFailRowNumbers(batchResultDTO.getFailRowNumbers().toString());
        uploadedInfo.setTotalCount(batchResultDTO.getTotalCount());
        uploadedInfo.setSuccessCount(batchResultDTO.getSuccessCount());
        uploadedInfo.setFailCount(batchResultDTO.getFailCount());

        // 파일 처리결과를 update한다.
        uploadedInfoRepository.save(uploadedInfo);

        return batchResultDTO;
    }

    @Override
    public List<UserDTO> readUserListFromFile(InputStream fileInputStream) throws Exception {
        List<String>  errorList = new ArrayList<String>();
        List<UserDTO> result = null;
        try( BufferedReader br = new BufferedReader(new InputStreamReader(fileInputStream));){
            AtomicInteger rowNumber = new AtomicInteger();
            result = br.lines()
                    .skip(1)
                    .map(line -> {
                        // split - 0.2sec
                        // List<String> delimResult = this.useSplit(line,",");
                        // tokenizer - 0.3 sec
                        // List<String> delimResult = this.useTokenizer(line,",");

                        // Split , Tokenizer 속도 비교테스트
                        List<String> delimResult = this.stringDelim(line,",", DELIM_SPLIT);
                        UserDTO.UserDTOBuilder dto = UserDTO.builder();
                        Integer currentRow = rowNumber.incrementAndGet();
                        if(delimResult.size() == 4 ){
                            Long id = null;
                            try{
                                id = Long.parseLong( delimResult.get(0) ) ;
                            }catch (NumberFormatException numberFormatException ){
                                // 콤마 갯수는 맞으나,
                                // id데이터가 정상적인 형태가 아님( 숫자타입이 아님 )
                                dto.rawData(line);
                            }
                            return dto
                                    .id(id)
                                    .firstName(delimResult.get(1))
                                    .lastName(delimResult.get(2))
                                    .email(delimResult.get(3))
                                    .rowNumber(currentRow)
                                    .build();
                        }else{
                            // 비정상케이스 - raw data만 담아 반환하자.
                            return UserDTO.builder().rawData(line).rowNumber(currentRow).build();
                        }
                    }).collect(Collectors.toList());

        }catch (FileNotFoundException fileNotFoundException){
            throw new BusinessException(ResultCodes.DATAFILE_NOT_FOUND);
        }catch (IOException ioException ){
            throw new IOException("파일을 읽는 중 오류가 발생하였습니다.");
        };

        return result;
    }

    @Override
    public void saveUserList(List<UserDTO> userList) throws Exception {

    }

    /**
     * Tokenizer로 문자열 분할
     * @param str
     * @param delim
     * @return
     */
    private List<String> useTokenizer(String str, String delim) {
        return Collections.list(new StringTokenizer(str, delim)).stream()
                .map(token -> (String) token)
                .collect(Collectors.toList());
    }

    /**
     * Split 으로 문자열 분할
     * @param str
     * @param delim
     * @return
     */
    private List<String> useSplit(String str, String delim){
        return Arrays.asList(str.split(delim));
    }

    /**
     * 문자열 분할
     * @param str
     * @param delim
     * @param type
     * @return
     */
    private List<String> stringDelim( String str, String delim , int type){
        return type == DELIM_TOKENIZER ? this.useTokenizer(str,delim) : this.useSplit(str,delim);
    }
}
