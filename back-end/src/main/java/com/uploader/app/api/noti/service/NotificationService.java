package com.uploader.app.api.noti.service;

import com.uploader.app.data.dto.BatchResultDTO;

public interface NotificationService {
    public BatchResultDTO currentBatchStatus(String hashValue ) throws Exception;
}
