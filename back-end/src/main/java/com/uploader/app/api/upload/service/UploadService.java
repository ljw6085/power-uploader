package com.uploader.app.api.upload.service;


import com.uploader.app.data.dto.UserDTO;
import com.uploader.app.data.dto.BatchResultDTO;
import org.springframework.web.multipart.MultipartFile;

import java.io.InputStream;
import java.util.List;

public interface UploadService {
    public BatchResultDTO checkUploadedFile(String hashValue )throws Exception;
    public void transferFile(MultipartFile file, String hashValue) throws Exception;
    public BatchResultDTO insertDataFromFile(String hashValue) throws Exception;
    public List<UserDTO> readUserListFromFile(InputStream fileInputStream) throws Exception;
    public void saveUserList(List<UserDTO> userList ) throws Exception;
}
