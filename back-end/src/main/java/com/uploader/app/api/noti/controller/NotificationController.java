package com.uploader.app.api.noti.controller;


import com.uploader.app.api.noti.service.NotificationService;
import com.uploader.app.data.dto.BatchResultDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.simp.annotation.SendToUser;
import org.springframework.messaging.simp.stomp.StompHeaderAccessor;
import org.springframework.web.bind.annotation.RestController;


@RestController
public class NotificationController {

    @Autowired NotificationService notificationService;

    @SendToUser // 전체 구독자에게 전송하는것이 Send한 대상에게만 응답한다. ( 파일별 진행상태 전송을 위해 )
    @MessageMapping("/batchUpdateStatus") // {prefix}/{brokerPrefix}/{messageMapping}
    public BatchResultDTO checkCurrentBatchStatus(StompHeaderAccessor headerAccessor) throws Exception {
        String hashValue = headerAccessor.getFirstNativeHeader("hashValue");
        BatchResultDTO batchResultDTO = notificationService.currentBatchStatus(hashValue);
        batchResultDTO.setHashValue(hashValue);
        return batchResultDTO;
    }

}
