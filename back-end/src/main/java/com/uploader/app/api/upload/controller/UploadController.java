package com.uploader.app.api.upload.controller;


import com.uploader.app.api.upload.service.UploadService;
import com.uploader.app.common.response.CommonResponse;
import com.uploader.app.common.response.ApiResponseUtils;
import com.uploader.app.data.dto.BatchResultDTO;
import com.uploader.app.data.dto.request.UploadFileRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/upload")
public class UploadController {

    @Autowired
    UploadService uploadService;


    @RequestMapping(value = "/check/{hashValue}" , method = RequestMethod.GET)
    public ResponseEntity<?> checkUploadedFile(@PathVariable String hashValue) throws Exception{
        BatchResultDTO batchResultDTO = uploadService.checkUploadedFile(hashValue);
        batchResultDTO.setHashValue(hashValue);
        return ApiResponseUtils.success(CommonResponse.of(batchResultDTO));
    }

    @RequestMapping(value = "/file" , method = RequestMethod.POST)
    public ResponseEntity<?> uploadFile(@ModelAttribute UploadFileRequest requestFileInfo )throws Exception{
        // 일단 업로드한 파일을 이동 후, DB에도 저장한다.
        uploadService.transferFile(requestFileInfo.getFile(), requestFileInfo.getHashValue());
        // 정상적으로 파일을 옮기고 DB처리하였으면 정상응답.
        return ApiResponseUtils.success();
    }

    @RequestMapping(value = "/data/{hashValue}" , method = RequestMethod.PUT)
    public ResponseEntity<?> uploadFile(@PathVariable  String hashValue )throws Exception{
        BatchResultDTO batchResultDTO = uploadService.insertDataFromFile(hashValue);
        batchResultDTO.setHashValue(hashValue);
        // 정상적으로 파일을 옮기고 DB처리하였으면 정상응답.
        return ApiResponseUtils.success(CommonResponse.of(batchResultDTO));
    }

}
