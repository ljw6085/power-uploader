package com.uploader.app.common.response;

import com.uploader.app.common.enums.ResultCodes;
import lombok.Getter;
import lombok.Setter;
import org.springframework.http.HttpStatus;
import org.springframework.validation.FieldError;

import java.util.ArrayList;
import java.util.List;


/**
 * 오류에 대한 공통 응답 structure 클래스
 */
@Getter
@Setter
public class ErrorResponse {
    private HttpStatus status;
    private String code;
    private String message;
    private Object body;
    private List<FieldError> errors;
    private String responseDateTime;


    private ErrorResponse(ResultCodes code, String message) {
        this.status = code.getStatus();
        this.code = code.getCode();
        this.message = message;
        this.errors = new ArrayList<>();
        this.body   = null;
    }

    private ErrorResponse(ResultCodes code) {
        this.status = code.getStatus();
        this.code = code.getCode();
        this.message = code.getMessage();
        this.errors = new ArrayList<>();
        this.body   = null;
    }

    private ErrorResponse(ResultCodes code, List<FieldError> errors) {
        this.status = code.getStatus();
        this.code = code.getCode();
        this.message = code.getMessage();
        this.errors = errors;
        this.body   = null;
    }
    private ErrorResponse(ResultCodes code, Object body) {
        this.status = code.getStatus();
        this.code = code.getCode();
        this.message = code.getMessage();
        this.errors = new ArrayList<>();
        this.body   = body;
    }


    public static ErrorResponse of(ResultCodes code, String message) {
        return new ErrorResponse(code, message);
    }

    public static ErrorResponse of(ResultCodes code) {
        return new ErrorResponse(code);
    }

    public static ErrorResponse of(ResultCodes code, List<FieldError> errors) {
        return new ErrorResponse(code, errors);
    }

    public static ErrorResponse of(ResultCodes code, Object body ){
        return new ErrorResponse(code, body);
    }


}
