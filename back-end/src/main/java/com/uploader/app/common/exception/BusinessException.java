/*
 * Copyright 2019 The Playce-SmartCity Project.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Revision History
 * Author            Date                  Description
 * ---------------  ----------------      ------------
 * Jeongho Baek      9월 19, 2019          First Draft.
 */
package com.uploader.app.common.exception;


import com.uploader.app.common.enums.ResultCodes;

/**
 * 비지니스 로직 중 발생하는 예외 클래스
 */
public class BusinessException extends RuntimeException {

  private ResultCodes errorCode;

  public BusinessException(String message) {
    super(message);
    this.errorCode = ResultCodes.INTERNAL_SERVER_ERROR;
  }
  public BusinessException(ResultCodes errorCode) {
    super(errorCode.getMessage());
    this.errorCode = errorCode;
  }
  public BusinessException(ResultCodes errorCode, String message) {
    super(message);
    this.errorCode = errorCode;
  }
  public ResultCodes getErrorCode() {
    return errorCode;
  }

}