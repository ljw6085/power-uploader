package com.uploader.app.common.response;

import com.uploader.app.common.enums.ResultCodes;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

@Component
public class ApiResponseUtils {
    private ApiResponseUtils() {

    }

    public static <T> ResponseEntity<Object> success() {
        return ApiResponseUtils.success(CommonResponse.of(ResultCodes.SUCCESS));
    }
    /**
     * 성공 프로세스에 대하여 ResponseEntity<CommonResponse> 정보를 리턴
     * @param commonResponse
     * @return
     */
    public static <T> ResponseEntity<Object> success(CommonResponse commonResponse) {
        commonResponse.setResponseDateTime(LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")));
        return new ResponseEntity<>(commonResponse, HttpStatus.OK);
    }
    public static <T> ResponseEntity<Object> success(Object object) {
        if( object instanceof CommonResponse ){
           return ApiResponseUtils.success((CommonResponse) object) ;
        }else{
            CommonResponse commonResponse = CommonResponse.of(object);
            commonResponse.setResponseDateTime(LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")));
            return new ResponseEntity<>(commonResponse, HttpStatus.OK);
        }
    }

    /**
     * 에러에 대하여 ResponseEntity<ErrorResponse> 정보를 리턴
     *
     * @param errorResponse
     * @return
     */
    public static <T> ResponseEntity<Object> error(ErrorResponse errorResponse) {
        errorResponse.setResponseDateTime(LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")));
       return new ResponseEntity<>(errorResponse , errorResponse.getStatus());
    }
}
