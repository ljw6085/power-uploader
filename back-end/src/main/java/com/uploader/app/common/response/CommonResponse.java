package com.uploader.app.common.response;

import com.uploader.app.common.enums.ResultCodes;
import lombok.Getter;
import lombok.Setter;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;

/**
 *   성공에 대한 공통 응답 structure 클래스
 */
@Getter
@Setter
public class CommonResponse {

    private HttpStatus status;
    private String code;
    private String message;
    private Object body;
    private String responseDateTime;

    private CommonResponse() {
        this.status = ResultCodes.SUCCESS.getStatus();
        this.code = ResultCodes.SUCCESS.getCode();
        this.message = ResultCodes.SUCCESS.getMessage();
    }

    private CommonResponse(Object body) {
        this.status = ResultCodes.SUCCESS.getStatus();
        this.code = ResultCodes.SUCCESS.getCode();
        this.message = ResultCodes.SUCCESS.getMessage();
        this.body = body;
    }

    private CommonResponse(ResultCodes code, Object body) {
        this.status = code.getStatus();
        this.message = code.getMessage();
        this.body = body;
    }

    private CommonResponse(ResultCodes code, String message, Object body) {
        this.status = code.getStatus();
        this.code = code.getCode();
        this.message = message;
        this.body = body;
    }

    public static CommonResponse of() {
        return new CommonResponse();
    }

    public static CommonResponse of(Object body) {
        return new CommonResponse(body);
    }

    public static CommonResponse of(ResultCodes code, Object body) { return new CommonResponse(code, body); }

    public static CommonResponse of(ResultCodes code, String message, Object body) { return new CommonResponse(code, message, body); }


}
