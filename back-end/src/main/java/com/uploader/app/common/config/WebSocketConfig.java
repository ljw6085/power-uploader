package com.uploader.app.common.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.messaging.simp.config.MessageBrokerRegistry;
import org.springframework.web.socket.config.annotation.EnableWebSocketMessageBroker;
import org.springframework.web.socket.config.annotation.StompEndpointRegistry;
import org.springframework.web.socket.config.annotation.WebSocketMessageBrokerConfigurer;

@Configuration
@EnableWebSocketMessageBroker
public class WebSocketConfig implements WebSocketMessageBrokerConfigurer {
    @Override
    public void configureMessageBroker(MessageBrokerRegistry registry) {
        // Broker Prefix : /queue
        // App->Server Prefix : /request
        // Server->App Prefix : /response
        // ==> {prefix}/{brokerPrefix}/{messageMapping} 형태로 데이터 전송
        registry.enableSimpleBroker("/queue");
        registry.setApplicationDestinationPrefixes("/request");
        registry.setUserDestinationPrefix("/response");
    }

    @Override
    public void registerStompEndpoints(StompEndpointRegistry registry) {
        registry
                // connection 생성시 /websocket 으로 요청
                .addEndpoint("/websocket")
                // 웹서버 cors 허용
                .setAllowedOrigins("http://localhost:8080")
                // SockJS를 사용하자.
                .withSockJS();
    }

}