package com.uploader.app.common.exception.handler;

import com.uploader.app.common.enums.ResultCodes;
import com.uploader.app.common.exception.BusinessException;
import com.uploader.app.common.response.ApiResponseUtils;
import com.uploader.app.common.response.ErrorResponse;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

/**
 * GlobalExceptionHandler : Controller 전역 예외처리 하는 핸들러
 */

@ControllerAdvice
public class GlobalExceptionHandler {

    /**
     * Controller 전역 예외처리 핸들러
     */
    @ExceptionHandler(Exception.class)
    protected ResponseEntity<Object> handleException(Exception ex) {
        ex.printStackTrace();
        // 개발자가 예측하지 못한 오류가 넘어오는경우,
        // ResultCodes에 대한 정보가 없기때문에
        // 다른 커스텀 익셉션과 다르게 아래와 같이 메시지가 있는지 확인 후 세팅

        // errorMessage 세팅 (기본메시지 또는 사용자정의 메시지 )
        String errorMessage = ex.getMessage() == null ?
                                    ResultCodes.INTERNAL_SERVER_ERROR.getMessage() : ex.getMessage();

        ErrorResponse errorResponse = ErrorResponse
                .of(ResultCodes.INTERNAL_SERVER_ERROR, errorMessage);

        return ApiResponseUtils.error(errorResponse);
    }

    /**
     * 비지니스 로직 중 발생하는 예외 클래스 (커스텀)
     */
    @ExceptionHandler(BusinessException.class)
    protected ResponseEntity<Object> handleBusinessException(BusinessException e) {
        return ApiResponseUtils.error(ErrorResponse.of(e.getErrorCode() , e.getMessage()));
    }

}


