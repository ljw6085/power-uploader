package com.uploader.app.common;

import com.uploader.app.data.dto.BatchResultDTO;
import org.springframework.stereotype.Component;

import java.util.HashMap;

@Component
public class CacheMap {
    private final HashMap<String, BatchResultDTO> data = new HashMap<String,BatchResultDTO>();

    public void put(String hashValue,BatchResultDTO batchResultDTO){
        this.data.put(hashValue, batchResultDTO);
    }
    public boolean exist( String hashValue ){
        return this.data.get(hashValue) != null;
    }
    public BatchResultDTO get( String hashValue ){
        return this.data.get(hashValue);
    }
    public void remove(String hashValue ){
        this.data.remove(hashValue);
    }
}
