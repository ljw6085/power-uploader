package com.uploader.app.common.enums;

import org.springframework.http.HttpStatus;

/**
 *  공통 응답코드 클래스
 */
public enum ResultCodes {

	// 성공 응답
	SUCCESS(HttpStatus.OK, "OK", "정상 처리 되었습니다."),

	// 공통 에러 응답
	BAD_REQUEST(HttpStatus.BAD_REQUEST, "ERROR."+HttpStatus.BAD_REQUEST, "잘못된 요청입니다."),
	INTERNAL_SERVER_ERROR(HttpStatus.INTERNAL_SERVER_ERROR, "ERROR."+HttpStatus.INTERNAL_SERVER_ERROR, "서비스를 처리할 수 없습니다."),

	// 비즈니스 에러
	ALREADY_INSERT_FILE_ERROR(HttpStatus.INTERNAL_SERVER_ERROR, "ERROR."+HttpStatus.INTERNAL_SERVER_ERROR, "이미 업로드된 파일입니다."),
	DATAFILE_NOT_FOUND(HttpStatus.INTERNAL_SERVER_ERROR, "ERROR."+HttpStatus.INTERNAL_SERVER_ERROR, "데이터파일이 존재하지 않습니다."),
	DATA_VALIDATE_ERROR(HttpStatus.INTERNAL_SERVER_ERROR, "ERROR."+HttpStatus.INTERNAL_SERVER_ERROR, "데이터 양식을 확인해주세요.")

	;


	private HttpStatus status;
	private String code;
	private String message;


	ResultCodes(HttpStatus status , String code, String message) {
		this.status = status;
		this.code = code;
		this.message = message;
	}

	public HttpStatus getStatus() {
		return status;
	}

	public String getCode() { return code; }

	public String getMessage() {
		return message;
	}



}
