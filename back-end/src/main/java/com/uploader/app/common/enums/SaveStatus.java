package com.uploader.app.common.enums;

public enum SaveStatus {
    NULL("NULL"),
    SUCCESS("SUCCESS"),
    FAIL("FAIL");

    String status;

    SaveStatus(String status) {
        this.status = status;
    }
    public String getStatus() {
        return status;
    }
}
