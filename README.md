# 파일 업로더
## Application 개요
### 개발 목적
* 사용자는 특정 서버에 자신의 로컬환경에 존재하는 csv파일을 브라우저에 파일을 드래그하여 일괄 입력 한다. 

### 제약사항
* csv파일만 가능하다.
* 같은내용의 파일은 1회만 업로드 가능하다.
* 단순히 데이터양식에 맞지않는 데이터가 존재한다면, 해당 내용은 무시하고 정상적인 데이터들을 저장한다.
* 이 때, 저장되지않은 데이터의 라인번호를 사용자에게 알려준다. 따로 데이터 작업후 새로운 파일을 만들어 재시도 해야한다.
* 식별시퀀스 중복이나, 서버상의 오류로 인한 작업중단은, 작업중단시점까지만 저장되고 그 이후 데이터는 저장되지않는다.
* 이 때, 마지막 저장데이터를 사용자에게 알려주니 그 이후 데이터부터 다시 파일을 만들어 재시도 가능하다.
* 위 내용처럼 최초 시도시 데이터 정합성에 맞지않는 오류가 발생한 경우, 해당 csv파일을 수정하여 재시도하여야 한다.
* 식별시퀀스는 임의대로 id값으로 지정하였다.
* 제공된 csv 파일의 형태인 id,firstname,lastname,email 의 형태의 데이터와 매핑된다.

## 매커니즘
* 이 어플리케이션에서는 파일의 ``해쉬값``을 key로 하여 유일한 값으로 판단한다.
* 브라우저에 파일 드랍시키는 순간 서버에 ``해쉬값``만 전달하여  파일정보(``이 시점에서는 해쉬key값만``)를 DB에 보관한다.(대기상태)
* 이 때 업로드 가능한 상태인지 사용자에게 확인시킨다.
* 확인 후 파일 업로드 요청한다.
	* 확인 후 파일 업로드 요청 시 서버에서는 파일의 대한 정보를 해쉬값을 기준으로 디비에 추가로 저장한다.
	* DB저장과 동시에 서버상에 파일을 복사하여 보관하고 , 곧바로 Batch Insert를 시작한다.
	* 이 때 특정 Count에 한번 (기본 1000건) commit을 시도하고 commit 시점마다  ``캐쉬맵``에 ``해쉬값``을 키값으로 진행상태값을 저장한다.
	* ExcuteBatch API 사용
* Ajax로 업로드요청을 했기 때문에 브라우저에서는 곧바로 서버에 ``해쉬값``을 들고가서 ``캐쉬멥``을 뒤져 진행중인 상태값을 받아온다.
	* 이 때에는 상대적으로 잦은 호출이 필요하기 때문에 (기본 100ms) Websocket(STOMP프로토콜)을 이용하였다.
* 상태값을 받아와 Vue컴포넌트에 주기적으로 업데이트해주고, 아까 요청했던 업로드 Ajax요청에 대한 응답이 오면 곧바로 Websocket Send를 종료한다.
* 최종 마지막으로 받아온 배치상태값을 가지고 사용자에게 ``총건수``/``성공건수``/``미처리건수(오류포함)``/``오류건수``/``오류메시지``/``오류발생라인번호``/``마지막으로입력된데이터`` 를 반환한다.
* 오류가 나더라도, 마지막으로 입력된 데이터와 오류가 난 데이터의 라인번호를 알려주므로 데이터 수정 후 재시도가 가능하다.

## 테스트코드 
* Back-end단의 단위테스트를 서비스별로 작성하였다.
* 크게 사용자 액션 기준으로 ``파일업로드직전``,``파일업로드요청``,``File Stream Read``, ``Data DB Insert`` 단계로 나누었다.
* 하지만 웹소켓통신에 대한 테스트코드를 미처 작성하지 못했다.
* 테스트코드에대한 지식이 많이 모자라 Front-end단의 테스트 코드 작성을 미처 하지 못했다.
* 업로드단의 기능들을 최대한 기능별로 쪼개어 놓아 단계별로 테스트는 가능할 것 같다.
	* 하지만 File과 서버에서 받아온 File의 진행상태를 지속적으로 비교하고 판단해야했기 때문에 ``공유변수($vm.data)``가 많이 사용되어 순수한 함수로는 기능하지 못한다.


# Front-End 
## 구성환경 
  * Vue.js (2.6.12)
	* vue-cli 3.x
	* eslint
	* prettier
  * axios 
  * sockjs-client
  * webstomp-client
  * md5
  * bootstrap
  * bootstrap-vue
  * dropzone

 ## 디렉토리 구조 
 ```
 fornt-end
	├── src
	│   ├── api  
	│   │   └── index.js             # API 통신 Module
	│   ├── components
	│   │   ├── UploadTargetFile.vue # 자식 컴포넌트 > 업로드파일 래핑 컴포넌트
	│   │   └── Uploader.vue         # 부모 컴포넌트 > Dropzone / WebSocket 
	│   ├── App.vue                  # App Root 컴포넌트
	│   └── main.js                  # App Entry point
    │	
	├── .eslintrc.js                 # 코드컨벤션/정리도구 : eslint/prettier 사용
	├── babel.config.js              # 트랜스파일러
	├── README.md
	└── package.json
```

# Back-end
## 구성환경
* Java 8
* Maven
* Spring Boot 2.4.3
* MariaDB/H2
* Spring Data JPA
* Spring Websocket 5.3.4
* JUnit 4.13
* Lombok
* RestAPI 기반 서버

### Server Port
```
server.port = 9090
```
### Docker command
```
docker run -p 3306:3306 --name mariadb -e MYSQL_ROOT_PASSWORD=abcd1234! -e MYSQL_DATABASE=YAPOKK_EVOLI -e MYSQL_USER=uploader -e MYSQL_PASSWORD=abcd1234! -d mariadb
```
### table query
```
create table uploaded_info (seq bigint not null, fail_count integer, fail_row_numbers varchar(255), file_hash_value varchar(255), file_path varchar(255), lasted_insert_raw_data varchar(255), lasted_status_message varchar(255), lasted_update_date datetime, status varchar(255), success_count integer, total_count integer, primary key (seq)) engine=InnoDB
create table user (id bigint not null, email varchar(255), first_name varchar(255), last_name varchar(255), primary key (id)) engine=InnoDB
```

### RDBM 접속시 유의사항
```
spring.datasource.url=jdbc:mysql://localhost:3306/YAPOKK_EVOLI?useSSL=false&rewriteBatchedStatements=true
```
 * url 작성시 ``rewriteBatchedStatements=true`` 파라미터 항목 필히 확인 추가.
 	* Mysql 사용시 배치쿼리 사용여부가 true 로 되어있어야 함.(excuteBatch API를 사용하기 때문에)

### 참고
* Controller 들은 항상 ``정상(HTTP 200)``을 응답한다는 기준으로 작성했다.
* 개발자가 예측할수 있는 오류는 ``성공(HTTP 200)``으로 응답하되 내용을 어플리케이션에서 정의한 코드로 내려보낸다.
* 그 외 예측할 수 없는 오류나, 비즈니스 로직에서 발생하는 오류상황들은 항상 throw Exception으로 응답 처리한다.
* JPA Entity 생성 시 테이블에대한 명확한 명세를 제시하지 않았다.
	* column명, 길이, 테이블명 등등 기본값으로 사용하였다.
* API Server에서 ``Temp``폴더 경로는  ``Application.properties``파일의 ``file.upload.temp.path`` 로 설정 돼있다.

### WebSocketConfig
* Endpoint
	* /websocket
* CORS Allow
	* http://localhost:8080
* APP->SERVER Prefix
	* /request
* SERVER->APP Prefix
	* /response
* MessageBroker Prefix
	* /queue

## 디렉토리 구조
```
back-end
  ├── src
  │  ├── main
  │  │  ├── java
  │  │  │  └── com.uploader.app
  │  │  │    ├── api
  │  │  │    │  ├── noti
  │  │  │    │  │  ├── controller
  │  │  │    │  │  │  └── NotificationController.java       # Message Broker Controller (Send<->Subscribe)
  │  │  │    │  │  └── service
  │  │  │    │  │     ├── impl
  │  │  │    │  │     │  └── NotificationServiceImpl.java   # 구독자에게 Push할 데이터를 만들어주는 서비스 
  │  │  │    │  │     └── NotificationService.java
  │  │  │    │  └── upload
  │  │  │    │     ├── controller
  │  │  │    │     │  └── UploadController.java             # 파일유효성검사/업로드/배치처리실행의 시작시점을 관장
  │  │  │    │     └── service
  │  │  │    │        ├── impl
  │  │  │    │        │  └── UploadServiceImpl.java         # 실질적인 유효성검사/업로드받은 파일복사/배치처리실행 등 비즈니스로직 수행
  │  │  │    │        └── UploadService.java
  │  │  │    ├── common
  │  │  │    │  ├── config
  │  │  │    │  │  └── WebSocketConfig.java                 # Websocket에대한 정보를 세팅 
  │  │  │    │  ├── enums
  │  │  │    │  │  ├── ResultCodes.java                     # 비즈니스로직 수행중 발생하는 코드정의
  │  │  │    │  │  └── SaveStatus.java                      # 업로드한 파일들의 상태값을 정의
  │  │  │    │  ├── exception
  │  │  │    │  │  ├── handler
  │  │  │    │  │  │  └── GlobalExceptionHandler.jav        # 전역적인 예외처리 핸들러.
  │  │  │    │  │  ├── BusinessException.java               # 비즈니스로직에서 발생시킬 커스텀 예외객체
  │  │  │    │  │  └── ValueReturnException.java            # 비즈니스로직에서 발생시킬 커스텀 예외객체( 값 전달 )
  │  │  │    │  ├── filter
  │  │  │    │  │  └── CORSFilter.java                      # Was(톰캣)용 Filter. CORS 허용 세팅 
  │  │  │    │  ├── response
  │  │  │    │  │  ├── ApiResponseUtils.java                # 응답시 사용하는 공통Entity 세팅 클래스 
  │  │  │    │  │  ├── CommonResponse.java                  # 정상 응답시 사용하는 공통 Response Entity
  │  │  │    │  │  └── ErrorResponse.java                   # 오류 응답시 사용하는 공통 Response Entity
  │  │  │    │  └── CacheMap.java
  │  │  │    ├── data
  │  │  │    │  ├── dto
  │  │  │    │  │  ├── request
  │  │  │    │  │  │  └── UploadFileRequest.java            # 파일 업로드시 사용되는 Rquest DTO
  │  │  │    │  │  ├── BatchResultDTO.java                  # 배치진행 상태를 담아 전달 가능한 DTO
  │  │  │    │  │  └── UserDTO.java                         # User 테이블에서 조회후 가공된 DTO
  │  │  │    │  ├── entity
  │  │  │    │  │  ├── UploadedInfo.java                    # 파일업로드히스토리를 관리하는 테이블에 매핑된 Entity (uploaded_info)
  │  │  │    │  │  └── User.java                            # user정보를 관리하는 테이블에 매핑된 Entity (user)
  │  │  │    │  └── repository
  │  │  │    │     ├── UploadedInfoRepository.java          # uploaded_info 테이블의 데이터에 접근/관리 한다.
  │  │  │    │     ├── UserRepository.java                  # user 테이블의 데이터에 접근/관리 한다.
  │  │  │    │     ├── UserRepositoryCustom.java            # user 테이블의 데이터에 접근/관리 한다.
  │  │  │    │     └── UserRepositoryImpl.java              # user 테이블의 데이터에 접근/관리 한다.
  │  │  │    └── BackEndApplication.java                    # Spring Boot EntryPoint Main Class
  │  │  └── resources
  │  │     ├── static
  │  │     ├── templates
  │  │     └── application.properties                       # Application에 필요한 상수들 정의 
  │  │ 
  │  └── test.java.com.uploader.app
  │      ├── api
  │      │  └── upload
  │      │      └── service
  │      │          ├── impl
  │      │          └── UploadServiceTest.java     # 파일업로드 관련 단위테스트
  │      ├── data
  │      │  └── repository
  │      │      └── UserRepositoryTest.java       # DB insert/select관련 단위테스트 
  │      └── BackEndApplicationTests.java
  └── pom.xml
```